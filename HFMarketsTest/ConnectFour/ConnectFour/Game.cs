﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{
    public sealed class Game
    {
        // Game Class Singleton.
        private static readonly Game instance = new Game();

        // Game Data.
        public CellType playerTurn = CellType.CROSS;
        public CellType winner = CellType.EMPTY;
        public bool gameFinished = false;

        // Ensuring Singleton pattern.
        static Game()
        {
        }
        private Game()
        {
        }
        public static Game Instance {
            get {
                return instance;
            }
        }


        /// <summary>
        /// Start New Game.
        /// </summary>
        public void NewGame()
        {
            playerTurn = CellType.CROSS;
            winner = CellType.EMPTY;

            gameFinished = false;

            Board.Instance.ClearBoard();

            RefreshView();
            GameLoop();

        }


        /// <summary>
        /// Refresh View.
        /// </summary>
        private void RefreshView()
        {
            Console.Clear();
            Board.Instance.PrintBoard();
        }


        /// <summary>
        /// Execute while game is running.
        /// </summary>
        private void GameLoop()
        {
            while (!gameFinished)
            {
                Console.Clear();

                if (playerTurn == CellType.CIRCLE)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(playerTurn + ", is your turn.");
                    Console.ResetColor();
                } else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(playerTurn + ", is your turn.");
                    Console.ResetColor();
                }

                Board.Instance.PrintBoard();

                // Get Player Input
                int selectedColumn = UserInput.RequestColumnInput();

                // Place Token.
                Board.Instance.PlaceToken(selectedColumn);

                // Check Game Session.
                CheckGameSessionEnded();
            }


            // Check if player wants to play again.
            if (UserInput.RequestPlayAgain())
            {
                NewGame();
            }
        }

        
        /// <summary>
        /// Change Turn.
        /// </summary>
        private void ChangeTurn()
        {
            if (playerTurn == CellType.CIRCLE)
                playerTurn = CellType.CROSS;
            else
                playerTurn = CellType.CIRCLE;
        }


        /// <summary>
        /// Check if a player has won, or board is full.
        /// </summary>
        private void CheckGameSessionEnded()
        {
            // Check Win
            bool isWon = Board.Instance.CheckWin();

            // A player has won or board is full.
            if (isWon)
            {
                RefreshView();

                if (winner == CellType.CIRCLE)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(winner + " won the game!!!!!!!!");
                    Console.ResetColor();
                }
                else if (winner == CellType.CROSS)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(winner + " won the game!!!!!!!!");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("It's a tie ...");
                    Console.ResetColor();
                }

                // Stop game loop.
                gameFinished = true;
            }
            else
            {
                // Else change turns.
                ChangeTurn();
            }
        }
    }
}
