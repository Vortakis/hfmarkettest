﻿using System;
using System.Collections.Generic;

namespace ConnectFour
{
    class Program
    {
        static void Main(string[] args)
        {
            // Start Game.
            Game.Instance.NewGame();
        }
    }
}
