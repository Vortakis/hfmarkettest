﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{
    public enum CellType
    {
        EMPTY,
        CROSS,
        CIRCLE
    }

    public sealed class Board
    {
        // Board Class Singleton.
        private static readonly Board instance = new Board();

        // Board Size.
        public const int HEIGHT = 6;
        public const int WIDTH = 7;

        // Board Grid.
        private CellType[,] boardCells = new CellType[WIDTH, HEIGHT];

        // Place tokens.
        private List<KeyValuePair<int, int>> placedTokens = new List<KeyValuePair<int, int>>();
        private List<KeyValuePair<int, int>> winningTokens = new List<KeyValuePair<int, int>>();

        // Board Columns that are full.
        private List<int> fullColumns = new List<int>();


        // Ensuring Singleton pattern.
        static Board()
        {
        }

        private Board()
        {
            ClearBoard();
        }
        public static Board Instance {
            get {
                return instance;
            }
        }


        /// <summary>
        /// Clear Board.
        /// </summary>
        public void ClearBoard()
        {
            for (int i = 0; i < WIDTH; i++)
            {
                for (int j = 0; j < HEIGHT; j++)
                {
                    boardCells[i, j] = CellType.EMPTY;
                }
            }

            placedTokens.Clear();
            fullColumns.Clear();
            winningTokens.Clear();
        }


        /// <summary>
        /// Prints Board on command line.
        /// </summary>
        public void PrintBoard()
        {
            Console.WriteLine();
            for (int i = 0; i < HEIGHT + 3; i++)
            {
                for (int j = 0; j < WIDTH + 2; j++)
                {
                    if (i == 0 || (i == HEIGHT + 1))
                        if (j != 0 && j != 8)
                            Console.Write("___");
                        else
                            Console.Write(" ");
                    else if (i == HEIGHT + 2)
                    {
                        if (j != 0 && j != 8)
                            Console.Write(" " + j + " ");
                        else
                            Console.Write(" ");
                    }
                    else if (j == 0 || j == WIDTH + 1)
                        Console.Write("|");
                    else
                    {
                        CellType currentCell = boardCells[j - 1, i - 1];
                        if (currentCell == CellType.EMPTY)
                            Console.Write(" - ");
                        else if (currentCell == CellType.CROSS)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write(" x ");
                            Console.ResetColor();
                        }
                        else if (currentCell == CellType.CIRCLE)
                        {
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Write(" o ");
                            Console.ResetColor();
                        }

                    }
                }
                Console.WriteLine();

            }
        }


        /// <summary>
        /// Check if column is full.
        /// </summary>
        /// <param name="columnId"></param>
        /// <returns></returns>
        public bool IsColumnFull(int columnId)
        {
            if (fullColumns.Contains(columnId))
                return true;
            else return false;
        }


        /// <summary>
        /// Place a token in specified column.
        /// </summary>
        /// <param name="columnId">Number of column to place token.</param>
        public void PlaceToken(int columnId)
        {

            for (int i = HEIGHT - 1; i >= 0; i--)
            {
                if (boardCells[columnId, i] == CellType.EMPTY)
                {
                    boardCells[columnId, i] = Game.Instance.playerTurn;
                    placedTokens.Add(new KeyValuePair<int, int>(columnId, i));

                    if (i == 0)
                    {
                        fullColumns.Add(columnId);
                    }
                    break;
                }
            }
        }

        
        /// <summary>
        /// Check Won or Full board.
        /// </summary>
        /// <returns></returns>
        public bool CheckWin()
        {
            if (CheckPlayerWin())
                return true;
            else if (CheckFullBoard())
                return true;
            else
                return false;
        }


        /// <summary>
        /// Check if Player has won.
        /// </summary>
        /// <returns>True if player won, false otherwise.</returns>
        private bool CheckPlayerWin()
        {
            foreach (var placedToken in placedTokens)
            {
                CellType currentToken = boardCells[placedToken.Key, placedToken.Value];
                winningTokens.Clear();

                // Horizontal Check
                if (placedToken.Key < WIDTH - 3)
                {
                    for (int i = 1; i < 4; i++)
                    {
                        if (boardCells[placedToken.Key + i, placedToken.Value] == currentToken)
                        {
                            if (i == 3)
                            {
                                for (int winIndex = 0; winIndex < 4; winIndex++)
                                {
                                    winningTokens.Add(new KeyValuePair<int, int>(placedToken.Key + winIndex, placedToken.Value));
                                }

                                Game.Instance.winner = currentToken;
                                return true;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                // Vertical Check
                if (placedToken.Value < HEIGHT - 3)
                {
                    for (int i = 1; i < 4; i++)
                    {
                        if (boardCells[placedToken.Key, placedToken.Value + i] == currentToken)
                        {
                            if (i == 3)
                            {
                                for (int winIndex = 0; winIndex < 4; winIndex++)
                                {
                                    winningTokens.Add(new KeyValuePair<int, int>(placedToken.Key, placedToken.Value + winIndex));
                                }

                                Game.Instance.winner = currentToken;
                                return true;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                // Diagonal Right Check
                if (placedToken.Key < WIDTH - 3 && placedToken.Value < HEIGHT - 3)
                {
                    for (int i = 1; i < 4; i++)
                    {
                        if (boardCells[placedToken.Key + i, placedToken.Value + i] == currentToken)
                        {
                            if (i == 3)
                            {
                                for (int winIndex = 0; winIndex < 4; winIndex++)
                                {
                                    winningTokens.Add(new KeyValuePair<int, int>(placedToken.Key + winIndex, placedToken.Value + winIndex));
                                }

                                Game.Instance.winner = currentToken;
                                return true;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                // Diagonal Left Check
                if (placedToken.Key > 2 && placedToken.Value < HEIGHT - 3)
                {
                    for (int i = 1; i < 4; i++)
                    {
                        if (boardCells[placedToken.Key - i, placedToken.Value + i] == currentToken)
                        {
                            if (i == 3)
                            {
                                for (int winIndex = 0; winIndex < 4; winIndex++)
                                {
                                    winningTokens.Add(new KeyValuePair<int, int>(placedToken.Key - winIndex, placedToken.Value + winIndex));
                                }

                                // Assign winner.
                                Game.Instance.winner = currentToken;
                                return true;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            // No winner found.
            return false;
        }


        /// <summary>
        /// Check whehter board is full.
        /// </summary>
        /// <returns>True if full, false otherwise.</returns>
        public bool CheckFullBoard()
        {
            if (fullColumns.Count != WIDTH)
                return false;
            else
            {
                Game.Instance.winner = CellType.EMPTY;
                return true;
            }
        }
    }
}
