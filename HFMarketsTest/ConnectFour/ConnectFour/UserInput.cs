﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{
    public static class UserInput
    {
        // Commands Available.
        private static List<char> columnCommands = new List<char>() { '1', '2', '3', '4', '5', '6', '7' };
        private static List<string> playAgainCommands = new List<string>() { "y", "n" };


        /// <summary>
        /// Request column id from user.
        /// </summary>
        /// <returns></returns>
        public static int RequestColumnInput()
        {
            Console.Write("Please type a valid column number (1-7): ");
            char input = Console.ReadKey().KeyChar;
            Console.WriteLine();

            if (!columnCommands.Contains(input))
            {
                return RequestColumnInput();

            }

            int intInput = int.Parse(input.ToString());

            if (Board.Instance.IsColumnFull(intInput - 1))
            {
                Console.Write("Column is Full! ");

                return RequestColumnInput();

            }

            return intInput - 1;
        }
        

        /// <summary>
        /// Request if player wants to playe again.
        /// </summary>
        /// <returns></returns>
        public static bool RequestPlayAgain()
        {
            Console.Write("Do you want to play again (y/n)? ");

            string input = Console.ReadKey().KeyChar.ToString().ToLower();
            Console.WriteLine();

            if (!playAgainCommands.Contains(input))
            {
                return RequestPlayAgain();
            }

            if (input.Equals("y"))
            {
                return true;
            }
            else
                return false;
        }
    }
}
